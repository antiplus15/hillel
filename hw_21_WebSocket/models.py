from peewee import Model, AutoField, CharField, BooleanField, DecimalField
from peewee import SqliteDatabase


db = SqliteDatabase('auction_db.sqlite3')


class Lot(Model):
    id = AutoField(primary_key=True, unique=True)
    is_active = BooleanField(default=True)
    name = CharField(max_length=255)
    owner = CharField(max_length=255)
    price = DecimalField(max_digits=10, decimal_places=2, auto_round=True, null=True)
    new_price = DecimalField(max_digits=10, decimal_places=2, auto_round=True, null=True)
    new_owner = CharField(max_length=255, null=True)

    class Meta:
        database = db

    def __str__(self):
        if self.new_price:
            return f'{self.name} - ${self.new_price}({self.new_owner})'
        else:
            return f'{self.name} - ${self.price}'


if __name__ == '__main__':
    db.create_tables([Lot])
