from aiohttp import web
import aioreloader
import handlers
import aiohttp_jinja2
import jinja2
from aiohttp_session import SimpleCookieStorage, setup
from models import Lot


def create_app():
    app = web.Application()

    app.ws_dict = dict()
    app.lots_dict = {lot.id: lot for lot in Lot.select()}

    aiohttp_jinja2.setup(app=app, loader=jinja2.FileSystemLoader('templates'))
    setup(app, SimpleCookieStorage())

    app.router.add_get(path="/", handler=handlers.index_get)
    app.router.add_post(path='/', handler=handlers.index_post)
    app.router.add_get(path='/offers', handler=handlers.all_offers_get, name='all_offers')
    app.router.add_post(path='/offers', handler=handlers.all_offers_post)
    app.router.add_get(path='/offers/{lot_id}', handler=handlers.auction_room_get)
    app.router.add_post(path='/offers/{lot_id}', handler=handlers.auction_room_post)
    app.router.add_get(path='/offers/{lot_id}/ws', handler=handlers.MessageBroker)
    return app


if __name__ == '__main__':
    app = create_app()
    aioreloader.start()
    web.run_app(app)
