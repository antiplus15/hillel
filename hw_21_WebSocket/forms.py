import wtforms
from models import Lot


class LotForm(wtforms.Form):
    name = wtforms.StringField(validators=[
        wtforms.validators.DataRequired()
    ])
    price = wtforms.DecimalField()

    def save(self, owner, is_active=True, new_price=None, new_owner=None,):
        Lot.create(is_active=is_active,
                   name=self.name.data,
                   owner=owner,
                   price=self.price.data,
                   new_price=new_price,
                   new_owner=new_owner
                   )
