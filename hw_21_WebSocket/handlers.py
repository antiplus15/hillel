import aiohttp_jinja2
from aiohttp import web
from aiohttp_session import get_session
from models import Lot
from forms import LotForm


@aiohttp_jinja2.template('index.html')
def index_get(request):
    return {}


@aiohttp_jinja2.template('index.html')
async def index_post(request):
    data = await request.post()
    if 'nickname' in data:
        session = await get_session(request)
        session['nickname'] = data['nickname']
        location = request.app.router['all_offers'].url_for()
        raise web.HTTPFound(location=location)

    return {'error': True}


@aiohttp_jinja2.template('all_offers.html')
async def all_offers_get(request):
    form = LotForm(request.query)
    session = await get_session(request)
    lots_dict = request.app.lots_dict
    return {'all_lots': [lot for lot in lots_dict.values()
                         if lot.is_active],
            'my_lots': [lot for lot in lots_dict.values()
                        if lot.owner == session['nickname']],
            'form': form
            }


@aiohttp_jinja2.template('all_offers.html')
async def all_offers_post(request):
    session = await get_session(request)
    form = LotForm(await request.post())
    if form.validate():
        form.save(owner=session['nickname'])
        request.app.lots_dict = {lot.id: lot for lot in Lot.select()}
        location = request.app.router['all_offers'].url_for()
        raise web.HTTPFound(location=location)

    lots_dict = request.app.lots_dict
    return {'all_lots': [lot for lot in lots_dict.values()
                         if lot.is_active],
            'my_lots': [lot for lot in lots_dict.values()
                        if lot.owner == session['nickname']],
            'form': form
            }


@aiohttp_jinja2.template('auction_room.html')
async def auction_room_get(request):
    lot_id = request.match_info.get('lot_id')
    lot = request.app.lots_dict.get(int(lot_id))
    session = await get_session(request)
    if lot.owner == session['nickname']:
        return aiohttp_jinja2.render_template(
            template_name='auction_room_owner.html',
            request=request,
            context={'lot': lot}
        )
    return {'lot': lot}


# @aiohttp_jinja2.template('redirect.html')
async def auction_room_post(request):
    sold_lot_id = int(request.match_info.get('lot_id'))
    sold_lot = request.app.lots_dict.get(sold_lot_id)
    session = await get_session(request)

    if sold_lot.owner != session['nickname']:
        raise web.HTTPUnauthorized()

    for client in request.app.ws_dict[sold_lot_id]:
        # send a message that the auction has ended
        if not client.closed:
            await client.send_json({
                'name': 'The auction is over',
                'message': f'sold to {sold_lot.new_owner} for {sold_lot.new_price}'
            })

    Lot.update(owner=sold_lot.new_owner,
               price=sold_lot.new_price,
               is_active=False,
               new_price=None,
               new_owner=None
               ) \
        .where(Lot.id == sold_lot_id) \
        .execute()
    updated_lot = Lot.get_by_id(sold_lot_id)
    request.app.lots_dict.update({sold_lot_id: updated_lot})
    location = request.app.router['all_offers'].url_for()
    raise web.HTTPFound(location=location)


class MessageBroker(web.View):
    def __init__(self, request):
        super().__init__(request)
        self.lot_id = int(self.request.match_info.get('lot_id'))
        self.lot = self.request.app.lots_dict.get(self.lot_id)
        self.current_price = int(self.lot.new_price if self.lot.new_price else self.lot.price)
        self.ws_list = self.request.app.ws_dict.setdefault(self.lot_id, [])

    async def get(self):
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        session = await get_session(request=self.request)
        nickname = session['nickname']
        self.ws_list.append(ws)
        async for message in ws:
            if message.data.isdigit() or message.data.replace(".", "", 1).isdigit():
                # if there is only a number in the message, raise the price
                new_price = int(float(message.data))

                if not self.request.app.lots_dict.get(self.lot_id).is_active:
                    # if lot is not active
                    # send a message only to yourself
                    await ws.send_json({
                        'name': 'The auction is over',
                        'message': 'bids are not accepted'
                    })
                    continue

                if new_price <= self.current_price:
                    # if prise < current price
                    # send a message only to yourself
                    await ws.send_json({
                        'name': 'Wrong price',
                        'message': 'Your price must be higher than the current one'
                    })
                    continue

                Lot.update(new_price=new_price,
                           new_owner=nickname
                           ) \
                    .where(Lot.id == self.lot_id) \
                    .execute()

                self.lot = Lot.get_by_id(self.lot_id)
                self.current_price = new_price

                self.request.app.lots_dict.update(
                    {self.lot_id: self.lot}
                )

                data_to_send = {
                    'name': f'{nickname} raised the price to',
                    'message': new_price
                }

            else:
                # if there is text in the message, send it to the chat
                data_to_send = {
                    'name': f'{nickname} says',
                    'message': message.data
                }
            for client in self.ws_list:
                if client.closed:
                    self.ws_list.remove(client)
                    continue
                await client.send_json(data_to_send)

        return ws
