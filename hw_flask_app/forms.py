import wtforms
from models import Author, Genre, Book
from datetime import date


class BookForm(wtforms.Form):
    name = wtforms.StringField(validators=[
        wtforms.validators.DataRequired()
    ])

    author = wtforms.SelectField()
    year = wtforms.IntegerField(validators=[
        wtforms.validators.NumberRange(max=date.today().year, message='Wrong year')
    ])

    genre = wtforms.SelectField()

    def save(self):

        Book.create(name=self.name.data,
                    author=self.author.data,
                    year=self.year.data,
                    genre=self.genre.data
                    )

    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, **kwargs):
        super().__init__(formdata, obj, prefix, data, meta, **kwargs)
        self.author.choices = [(x.id, x.name) for x in Author.select()]
        self.genre.choices = [(x.id, x.name) for x in Genre.select()]


class AuthorForm(wtforms.Form):
    name = wtforms.StringField(validators=[
        wtforms.validators.DataRequired()
    ])

    descriptin = wtforms.TextAreaField(validators=[
        wtforms.validators.Optional(strip_whitespace=True),
        wtforms.validators.DataRequired()
    ])

    def save(self):
        Author.create(name=self.name.data,
                      description=self.descriptin.data
                      )


class GenreForm(wtforms.Form):
    name = wtforms.StringField(validators=[
        wtforms.validators.DataRequired()
    ])

    def save(self):
        Genre.create(name=self.name.data)

