from peewee import Model, AutoField, CharField, TextField, ForeignKeyField, IntegerField
from peewee import SqliteDatabase
from conf import db_name

db = SqliteDatabase(db_name)


class Author(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)
    description = TextField(null=True)

    class Meta:
        database = db


class Genre(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)

    class Meta:
        database = db


class Book(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)
    year = IntegerField()
    author = ForeignKeyField(Author, backref='books')
    genre = ForeignKeyField(Genre, backref='genres')

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([Author, Genre, Book])
