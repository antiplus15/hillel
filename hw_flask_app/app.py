from flask import Flask, render_template, request, redirect, abort
from faker import Faker
import requests
from peewee import DoesNotExist
from models import Book, Author, Genre
from forms import BookForm, AuthorForm, GenreForm

app = Flask(__name__)


@app.route('/requirements/')
def requirements():
    with open('../requirements.txt') as f:
        data = [x for x in f.readlines()]
    return render_template('requirements.html', data=data)


@app.route('/generate-users/')
def generate_users():
    number = int(request.values.get('number', 100))
    users = [{'name': Faker().name(),
              'email': Faker().email()
              }
             for _ in range(number)
             ]

    return render_template(
        'generate-users.html', **{
            'users': users,
            'number': number,
        }
    )


@app.route('/space/')
def space():
    response = requests.get("http://api.open-notify.org/astros.json")
    data = []
    if response.status_code == 200:
        data = response.json()

    return render_template('space.html', **data)


# Lesson 5 home work

@app.route('/library/')
def book_list():
    title = 'All books'
    books = Book.select().join_from(Book, Author).join_from(Book, Genre)
    return render_template('lib/list_books.html',
                           books=books, title=title)


@app.route('/library/author/<int:author_id>/')
def book_by_author(author_id):
    try:
        title = f"Author: {Author.get_by_id(author_id).name}"
    except DoesNotExist:
        return abort(404)

    books = Book.select().where(Book.author == author_id)
    return render_template('lib/list_books.html',
                           books=books, title=title)


@app.route('/library/genre/<int:genre_id>/')
def books_by_genre(genre_id):
    try:
        title = f"Genre: {Genre.get_by_id(genre_id).name}"
    except DoesNotExist:
        return abort(404)

    books = Book.select().where(Book.genre == genre_id)
    return render_template('lib/list_books.html',
                           books=books, title=title)


@app.route('/library/year/<int:year>/')
def books_by_year(year):
    books = Book.select().where(Book.year == year)
    if not books:
        return abort(404)
    title = f"Books for the year {year}"
    return render_template('lib/list_books.html',
                           books=books, title=title)


@app.route('/library/update/book/<int:book_id>/', methods=['GET', 'POST'])
def update_book(book_id):
    try:
        book = Book.get_by_id(book_id)
    except DoesNotExist:
        return abort(404)

    title = f"Update book: {book.name}"
    form = BookForm(request.form, book)
    if request.method == 'POST':
        if not form.validate():
            return render_template('lib/update_book.html',
                                   form=form, title=title, id=book_id), 400
        Book.update(request.form).where(Book.id == book_id).execute()
        return redirect('/library/')

    return render_template('lib/update_book.html',
                           form=form, title=title, id=book_id)


@app.route('/library/create/book/', methods=['GET', 'POST'])
def add_book():
    title = 'Add book'
    form = BookForm(request.form)
    if request.method == 'POST':
        if not form.validate():
            return render_template('lib/create.html',
                                   form=form, title=title), 400
        form.save()
        return redirect('/library/')

    return render_template('lib/create.html',
                           form=form, title=title)


@app.route('/library/create/author/', methods=['GET', 'POST'])
def add_author():
    title = 'Add author'
    form = AuthorForm(request.form)
    if request.method == 'POST':
        if not form.validate():
            return render_template('lib/create.html',
                                   form=form, title=title), 400
        form.save()
        return redirect('/library/')
    return render_template('lib/create.html',
                           form=form, title=title)


@app.route('/library/create/genre/', methods=['GET', 'POST'])
def add_genre():
    title = 'Add genre'
    form = GenreForm(request.form)
    if request.method == 'POST':
        if not form.validate():
            return render_template('lib/create.html',
                                   form=form, title=title), 400
        form.save()
        return redirect('/library/')
    return render_template('lib/create.html',
                           form=form, title=title)


@app.route('/library/remove_book/', methods=['POST'])
def remove_book():
    Book.delete().where(Book.id == int(request.form['id'])).execute()
    return redirect('/library/')


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )
