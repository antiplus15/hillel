import pytest
from faker import Faker
from datetime import date
from unittest.mock import patch

fake = Faker()
# test_db_name = "peewee_lib__test.sqlite3"


@pytest.fixture
def get_client_and_models():
    with patch('conf.db_name', ':memory:'):
        import models
        if not models.db.get_tables():
            models.db.create_tables([models.Author, models.Genre, models.Book])
        from app import app
        with app.test_client() as client:
            yield client, models


@pytest.fixture(
    name='add_author',
    params=[
        {'name': 'Some Author', 'description': fake.text()},
        {'name': 'Other Author', 'description': ''},
        pytest.param(
            {'name': '', 'description': fake.text()},
            marks=pytest.mark.xfail(reason="wrong data")
        ),
        pytest.param(
            {'name': '', 'description': ''},
            marks=pytest.mark.xfail(reason="wrong data")
        )
    ],
    ids=[
        'rand_name&rand_description',
        'rand_name&no_description',
        'no_name&rand_description',
        'no_name&no_description'
    ])
def add_author(request, get_client_and_models):
    client, models = get_client_and_models
    data = request.param
    return client, models, data


@pytest.fixture(
    name='add_genre',
    params=[
        {'name': 'Some genre'},
        pytest.param(
            {'name': ''},
            marks=pytest.mark.xfail(reason="wrong data")
        ),
    ],
    ids=[
        'valid_name',
        'no_name',
    ])
def add_genre(request, get_client_and_models):
    client, models = get_client_and_models
    data = request.param
    return client, models, data


@pytest.fixture(
    name='add_book',
    params=[
        {'name': f'book_{fake.word()}',
         'author': '1',
         'year': '1000',
         'genre': '1'
         },
        {'name': f'book_{fake.word()}',
         'author': '2',
         'year': str(date.today().year),
         'genre': '1'
         },
        pytest.param(
            {'name': f'book_{fake.word()}',
             'author': '100',
             'year': str(date.today().year),
             'genre': '1'
             },
            marks=pytest.mark.xfail(reason="wrong data")
        ),
        pytest.param(
            {'name': f'book_{fake.word()}',
             'author': '1',
             'year': str(date.today().year),
             'genre': '100'
             },
            marks=pytest.mark.xfail(reason="wrong data")
        ),
        pytest.param(
            {'name': f'book_{fake.word()}',
             'author': '1',
             'year': str(date.today().year + 1),
             'genre': '1'
             },
            marks=pytest.mark.xfail(reason="wrong data")
        ),
        pytest.param(
            {'name': '',
             'author': '',
             'year': '',
             'genre': ''
             },
            marks=pytest.mark.xfail(reason="wrong data")
        ),
    ],
    ids=[
        'valid_data&first_author',
        'valid_data&second_author',
        'invalid_data&wrong_author',
        'invalid_data&wrong_genre',
        'invalid_data&wrong_year',
        'no_data',
    ])
def add_book(request, get_client_and_models):
    client, models = get_client_and_models
    data = request.param
    return client, models, data


@pytest.fixture
def update_book(get_client_and_models):
    client, models = get_client_and_models
    new_data = {'name': f'updated_book',
                'author': '2',
                'year': '2000',
                'genre': '1'
                }
    return client, models, new_data
