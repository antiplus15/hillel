def test_404(get_client_and_models):
    client, _ = get_client_and_models
    print(404)
    response = client.get('/')
    assert response.status_code == 404


def test_requirements(get_client_and_models):
    client, _ = get_client_and_models
    response = client.get('/requirements/')
    with open('../requirements.txt', 'rb') as f:
        for line in f.readlines():
            assert line in response.data


def test_add_author(add_author):
    print('add author')
    client, models, data = add_author
    response = client.post('/library/create/author/', data=data)

    # 400 == invalid data
    assert response.status_code != 400

    # checking added author in database
    added_author = models.Author.select().where(models.Author.name == data['name'])
    assert added_author.exists()

    # 302 == redirect to /library/
    assert response.status_code == 302


def test_add_genre(add_genre):
    print('add genre')
    client, models, data = add_genre
    response = client.post('/library/create/genre/', data=data)

    # 400 == invalid data
    assert response.status_code != 400

    # checking added genre in database
    added_genre = models.Genre.select().where(models.Genre.name == data['name'])
    assert added_genre.exists()

    # 302 == redirect to /library/
    assert response.status_code == 302


def test_add_book(add_book):
    print('add book')
    client, models, data = add_book
    response = client.post('/library/create/book/',
                           data=data,
                           follow_redirects=True
                           )

    # 400 == invalid data
    assert response.status_code != 400

    # checking the added book in the database
    added_book = models.Book.select().where(models.Book.name == data['name'])
    assert added_book.exists()

    # redirect to /library/
    # checking the added data in the library
    for value in data.values():
        assert value.encode() in response.data


def test_update_book(update_book):
    from peewee import fn
    client, models, new_data = update_book
    book = models.Book.select().order_by(fn.Random()).get()
    book_id = book.id
    book_old_name = book.name
    response = client.post(f'/library/update/book/{book_id}/',
                           data=new_data,
                           follow_redirects=True
                           )

    # 400 == invalid data
    assert response.status_code != 400

    # check updated book in /library/
    assert new_data['name'].encode() in response.data

    # check updated book in db
    assert models.Book.get_by_id(book_id).name != book_old_name
    assert models.Book.get_by_id(book_id).name == new_data['name']


def test_delete_book(get_client_and_models):
    from peewee import fn
    client, models = get_client_and_models
    book = models.Book.select().order_by(fn.Random()).get()
    response = client.post('/library/remove_book/',
                           data={'id': book.id},
                           follow_redirects=True
                           )

    # redirect to /library/
    assert response.status_code == 200

    # checking deleted book in /library/
    assert book.name.encode() not in response.data

    # checking deleted book in database
    assert not models.Book.get_or_none(models.Book.id == book.id)
