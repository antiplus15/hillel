from urllib.parse import urlsplit, urlunsplit
from pymongo import MongoClient
from scrapy.spiders import Spider, Request


client = MongoClient()


class MyPars(Spider):
    name = 'name'
    start_urls = [
        'https://epicentrk.ua/actions/tovary-dlya-zashchity-i-profilaktiki-ot-koronavirusa.html',
        'https://epicentrk.ua/shop/velosipedy/',
    ]
    collection = client.epicentrk_db.parsed_collection
    save_without_price = False

    def parse(self, response, **kwargs):
        for link in response.css('a.card__photo'):
            yield Request(link.attrib['href'], self.goods_pars)

        next_page = response.css('.pagination__button--next::attr(href)').get()
        if next_page:
            self_url = urlsplit(response.url)
            next_url = urlsplit(next_page)
            concat_url = urlunsplit(
                (self_url.scheme,
                 self_url.netloc,
                 next_url.path,
                 next_url.query,
                 next_url.fragment
                 )
            )
            yield Request(concat_url, self.parse)

    def goods_pars(self, response, **kwargs):
        price = response.css('.p-price__main::text').get()
        if self.save_without_price or price:
            data = {
                        'name': response.css('.p-header__title::text').get(),
                        'price': str(price).lstrip(),
                        'spec': []
                    }

            for spec_part in response.css('.p-char__item'):
                key = ''.join(spec_part.css('.p-char__name ::text').getall()).strip().rstrip(':')
                if not key:
                    continue
                value = ''.join(spec_part.css('.p-char__value ::text').getall()).strip().replace('\xa0', '')
                data['spec'].append({
                    key: value
                })
            self.collection.insert_one(data)
            yield data




