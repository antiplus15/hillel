"""lesson6_home_work URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from core import views as core_view
from api import views as api_view
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('', core_view.IndexView.as_view(), name='index'),
    path('csv/', core_view.StudentsCSV.as_view()),
    path('admin/', admin.site.urls),
    path('group/<int:pk>/', core_view.GroupDetailView.as_view(), name='group_detail'),
    path('group/create/', core_view.CreateGroupView.as_view(), name='group_create'),
    path('group/update/<int:pk>/', core_view.UpdateGroupView.as_view(), name='group_update'),
    path('teacher/', core_view.TeacherView.as_view(), name='teacher_all'),
    path('teacher/<int:pk>/', core_view.TeacherDetailView.as_view(), name='teacher_detail'),
    path('teacher/create/', core_view.CreateTeacherView.as_view(), name='teacher_create'),
    path('teacher/update/<int:pk>/', core_view.UpdateTeacherView.as_view(), name='teacher_update'),
    path('student/', core_view.StudentView.as_view(), name='student_all'),
    path('student/create/', core_view.CreateStudentView.as_view(), name='student_create'),
    path('student/update/<int:pk>/', core_view.UpdateStudentView.as_view(), name='student_update'),
    path('student/delete/<int:pk>/', core_view.DeleteStudentView.as_view(), name='student_delete'),
    path('contact_us/', core_view.ContactUsView.as_view(), name='contact_us'),
    path('contact_us/done/', TemplateView.as_view(template_name='contact_us_done.html'), name='contact_us_done'),

    path('__debug__/', include(debug_toolbar.urls)),

    # path('api-auth/', include('rest_framework.urls')),
    path('api/get_token/', obtain_auth_token, name='api_get_token'),
    path('api/students/', api_view.StudentView.as_view({'get': 'list',
                                                        'post': 'create'})),
    path('api/students/<int:pk>/', api_view.StudentView.as_view({'put': 'update'})),
    path('api/groups/', api_view.GroupView.as_view({'get': 'list',
                                                    'post': 'create'})),
    path('api/groups/<int:pk>/', api_view.GroupView.as_view({'get': 'retrieve',
                                                             'put': 'update',
                                                             'delete': 'destroy'})),
    path('api/teachers/', api_view.GroupView.as_view({'get': 'list',
                                                      'post': 'create'})),
    path('api/teachers/<int:pk>/', api_view.GroupView.as_view({'get': 'retrieve',
                                                               'put': 'update',
                                                               'delete': 'destroy'})),
]
