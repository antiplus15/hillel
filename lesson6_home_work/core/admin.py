from django.contrib import admin
from core.models import Group, Student, Teacher, Logger, ExchangeRate

# Register your models here.
admin.site.register(Teacher)
admin.site.register(Group)
admin.site.register(Student)
admin.site.register(Logger)
admin.site.register(ExchangeRate)

