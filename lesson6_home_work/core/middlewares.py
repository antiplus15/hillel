from django.utils.deprecation import MiddlewareMixin
from django.utils import timezone
from time import time
from core.models import Logger


class LogMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.execution_time = time()

    def process_response(self, request, response):
        request.execution_time = time() - request.execution_time
        if not request.path.startswith('/admin'):
            Logger.objects.create(
                created=timezone.now(),
                request_execution_time=request.execution_time,
                request_path=request.path,
                request_method=request.method
            )
        return response
