from django.db import models
from django.utils import timezone



class Student(models.Model):
    name = models.CharField(max_length=255)
    date_of_birth = models.DateField()

    @property
    def age(self):
        today = timezone.now().date()
        dob = self.date_of_birth
        return today.year - dob.year - (today < dob.replace(year=today.year))

    def __str__(self):
        return self.name


class Teacher(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()

    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=255)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    students = models.ManyToManyField(Student)

    @property
    def get_statistic(self):
        ages = [s.age for s in self.students.all()]
        if not ages:
            return {'student_count': 0}
        return {'max_age': max(ages),
                'min_age': min(ages),
                'avg_age': sum(ages) // len(ages),
                'student_count': len(ages),
                }

    def __str__(self):
        return self.name


class Logger(models.Model):
    created = models.DateTimeField()
    request_execution_time = models.FloatField()
    request_method = models.CharField(max_length=255)
    request_path = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'logs'
        verbose_name_plural = 'logs'

    def __str__(self):
        return f'[{self.created.strftime("%d/%m/%Y %H:%M:%S")}] "{self.request_method} {self.request_path}"'


class ExchangeRate(models.Model):
    USD = 840
    EUR = 978
    RUB = 643
    CURRENCY_CHOICE = (
        (USD, 'USD'),
        (EUR, 'EUR'),
        (RUB, 'RUB')
    )
    currency = models.SmallIntegerField(choices=CURRENCY_CHOICE, null=False)
    buy = models.DecimalField(max_digits=8, decimal_places=4, null=False)
    sale = models.DecimalField(max_digits=8, decimal_places=4, null=False)
    created = models.DateTimeField(auto_now_add=True)
    source = models.CharField(max_length=255, null=False)

    def __str__(self):
        return f'{self.get_currency_display()} - ({self.buy}; {self.sale})' \
               f'[{self.created.strftime("%d/%m/%Y %H:%M:%S")}]"{self.source}"'
