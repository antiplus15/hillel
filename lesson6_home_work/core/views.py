from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView, CreateView, DeleteView, UpdateView, FormView, View
from django.views.generic.list import ListView
from django.urls import reverse_lazy
import csv
from django.conf import settings
from os import listdir as os_listdir

from core.forms import GroupForm, TeacherForm, StudentForm, ContactUsForm
from core.models import Group, Teacher, Student


class IndexView(ListView):
    template_name = "index.html"
    queryset = Group.objects.get_queryset().\
        prefetch_related('students').\
        annotate(teacher_name=F('teacher__name'))

    # def get_queryset(self):
    #     return Group.objects.get_queryset(
    #     ).prefetch_related('students').annotate(
    #         teacher_name=F('teacher__name'),
    #     )


class TeacherView(ListView):
    template_name = "teacher.html"
    model = Teacher


class StudentView(ListView):
    template_name = "student.html"

    def get_queryset(self):
        return Student.objects.order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        img_path = '/core/images/students_photo/'
        img_names = os_listdir(str(settings.STATIC_ROOT) + img_path)
        context['list_img_path'] = [img_path + name for name in img_names]
        return context



class GroupDetailView(DetailView):
    template_name = 'detail_group.html'
    queryset = Group.objects.get_queryset().\
        select_related('teacher').\
        prefetch_related('students')



class TeacherDetailView(DetailView):
    template_name = 'detail_teacher.html'
    model = Teacher


class MyCreateView(CreateView):
    success_url = reverse_lazy('index')
    template_name = 'create_update.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['button_name'] = 'Create'
        return context


class MyUpdateView(UpdateView):
    template_name = 'create_update.html'
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['button_name'] = 'Update'
        return context


class CreateGroupView(MyCreateView):
    model = Group
    form_class = GroupForm


class CreateTeacherView(MyCreateView):
    model = Teacher
    form_class = TeacherForm


class CreateStudentView(MyCreateView):
    model = Student
    form_class = StudentForm

    def get_template_names(self):
        if self.request.is_ajax():
            return ['includes/short_student_create.html']
        return [self.template_name]

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            context = {'object_list': Student.objects.order_by('-id')}
            return render(template_name=['includes/short_student_data_table.html'], context=context, request=self.request)
        else:
            super(CreateStudentView, self).form_valid(form)


class UpdateGroupView(MyUpdateView):
    model = Group
    form_class = GroupForm


class UpdateTeacherView(MyUpdateView):
    model = Teacher
    form_class = TeacherForm


class UpdateStudentView(MyUpdateView):
    model = Student
    form_class = StudentForm

    def get_template_names(self):
        if self.request.is_ajax():
            # print(dir(self.request))
            return ['includes/short_student_update.html']
        return [self.template_name]

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            context = {'object_list': Student.objects.order_by('-id')}
            return render(template_name=['includes/short_student_data_table.html'], context=context, request=self.request)
        else:
            super(UpdateStudentView, self).form_valid(form)


class DeleteStudentView(DeleteView):
    model = Student
    success_url = reverse_lazy('index')
    template_name = 'delete.html'

    def get_template_names(self):
        if self.request.is_ajax():
            return ['includes/short_student_delete.html']
        return [self.template_name]

    def delete(self, request, *args, **kwargs):
        print(request)
        if self.request.is_ajax():
            self.get_object().delete()
            context = {'object_list': Student.objects.order_by('-id')}
            return render(template_name=['includes/short_student_data_table.html'], context=context, request=self.request)
        else:
            return super().delete(request, *args, **kwargs)


class ContactUsView(FormView):
    template_name = 'contact_us.html'
    success_url = reverse_lazy('contact_us_done')
    form_class = ContactUsForm

    def form_valid(self, form):
        form.save()
        return super(ContactUsView, self).form_valid(form)


class StudentsCSV(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="students.csv"'
        writer = csv.writer(response)
        writer.writerow([field.name for field in Student._meta.fields])
        writer.writerows(Student.objects.all().values_list())
        return response
