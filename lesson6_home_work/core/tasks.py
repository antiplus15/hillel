from django.conf import settings
from lesson6_home_work.celery import app
from django.core.mail import send_mail
from django.utils import timezone
from core.models import Logger
from rest_framework.authtoken.models import Token


@app.task
def send_contact_us_email(name, message, email):
    send_mail(
        f'{name} leave message for you!',
        f'{message} \n Email: {email}',
        settings.SUPPORT_EMAIL_FROM,
        [settings.ADMIN_EMAIL],
        fail_silently=False,
    )


@app.task
def delete_logs_older_7_days():
    seven_days_ago = timezone.now() - timezone.timedelta(days=7)
    Logger.objects.filter(created__lt=seven_days_ago).delete()


@app.task
def update_drf_tokens():
    old_tokens = Token.objects.all().select_related('user')
    new_tokens = [Token(user=token.user, key=Token.generate_key())
                  for token in old_tokens]
    old_tokens._raw_delete(old_tokens.db)
    Token.objects.bulk_create(new_tokens)
