$(document).ready(function () {

    let add_student = $('div#add_student');

    $.get('/student/create/', function (data){
            add_student.append($(data).hide());
        });

    add_student.on('click', '.add_button', function () {
        console.log('add');
        $(this).hide();
        $(this).siblings().show();
    });

    add_student.on('click', '.cancel_create', function () {
        console.log('cancel add');
        let form = $(this).closest('#create_form');
        form.trigger('reset');
        form.hide();
        form.siblings().show();
    });

    add_student.on('click', '.create_student', function (event) {
        console.log('create')
        // event.preventDefault();
        let form = $(this).closest('#create_form');
        let dataToSend = {};
        form.serializeArray().map(function(x){dataToSend[x.name] = x.value;});
        console.log(dataToSend);
        $.post('/student/create/', dataToSend, function (data) {
            if($('.error-msg', $(data)).length) {
                $('#create_form').html(data);
            } else {
                $('.cancel_create').trigger('click');
                $('#data-table').html(data);
            }
        });
    });

    let table = $('table')

    table.on('click', "button.update", function (){
        console.log('update');
        let student = $(this).closest($('.student'));
        student.attr('student_id', student.find('.student_id').text())
        $.get('update/'+student.attr('student_id'), function (data){
            student.closest('table').find('button.cancel_update').trigger('click');
            student.find('.update_form').html(data).show()
            student.find('.student_row').hide()
        })
    });

    table.on('click', "button.cancel_update", function (){
        console.log('cancel')
        let student = $(this).closest($('.student'));
        student.find('.update_form').hide()
        student.find('.student_row').show()
    });

    table.on('click', "button.update-student", function (){
        console.log('update student')
        let student = $(this).closest($('.student'));
        let student_id = student.attr('student_id')
        let form = student.find('#form_'+student_id);
        let dataToSend = {}
        form.serializeArray().map(function(x){dataToSend[x.name] = x.value;});
        console.log(dataToSend)
        $.post('/student/update/'+student_id+'/', dataToSend, function (data) {
            if ($('.error-msg', $(data)).length) {
                $('#update_form').html(data);
            } else {
                $('#data-table').html(data);
            }
        });
    });

    table.on('click', "button.delete-student", function (){
        console.log('delete student')
        let student = $(this).closest($('.student'));
        let student_id = student.attr('student_id');
        $.get('/student/delete/'+student_id+'/', function (data) {
            let delete_modal = $('div.delete_student_modal')
            delete_modal.html(data).show();
            $('body').append($('<div class="shadow"></div>'));
            delete_modal.find('form').attr('student_id', student_id)
        });
    });

    let delete_modal = $('div.delete_student_modal')

    delete_modal.on('click', 'button.cancel_delete', function (){
        console.log('cancel_delete')
        $(this).closest('.delete_student_modal').hide()
        $(this).closest('#delete_form').remove()
        $('.shadow').remove()
    });

    delete_modal.on('click', 'button.delete', function () {
        console.log('delete')
        let form = $(this).closest('#delete_form')
        let student_id = form.attr('student_id')
        let dataToSend = {}
        form.serializeArray().map(function (x) {
            dataToSend[x.name] = x.value;
        });
        console.log(dataToSend)
        $.post('/student/delete/'+student_id+'/', dataToSend, function (data) {
            form.closest('.delete_student_modal').hide()
            $('.shadow').remove()
            form.remove()
            $('#data-table').html(data);
        })
    });

});



