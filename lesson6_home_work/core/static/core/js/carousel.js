$(document).ready(function () {
    let all_images = $('.img_set').children();
    let image = all_images.first().show();
    let carousel = $('div.carousel');

    function flip_back_all(func) {
        image.animate({width: 'toggle'}, 200);
        image = image.prev().animate({width: 'toggle'}, 200);
        setTimeout(function () {
            if (image.prev().length){
                flip_back_all();
            }
        }, 200)
    }

    function flip_forward_all(func) {
        image.animate({width: 'toggle'}, 200);
        image = image.next().animate({width: 'toggle'}, 200);
        setTimeout(function () {
            if (image.next().length){
                flip_forward_all();
            }
        }, 200)
    }

    carousel.on('click', '.img_next', function () {
        if (!$(':animated').length) {
            if (!image.next().length) {
                flip_back_all();
            } else {
                image.animate({width: 'toggle'}, 500);
                image = image.next().animate({width: 'toggle'}, 500);
            }
        }
    });

    carousel.on('click', '.img_prev', function () {
        if (!$(':animated').length) {
            if (!image.prev().length) {
                flip_forward_all();
            } else {
                image.animate({width: 'toggle'}, 500);
                image = image.prev().animate({width: 'toggle'}, 500);
            }
        }
    });
});
