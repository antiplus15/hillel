import urllib.request
from django.core.management.base import BaseCommand
from core.models import ExchangeRate
import requests
from decimal import Decimal
from requests_html import HTMLSession
import lxml.html as lh
import PyPDF2
import urllib
from io import BytesIO
from re import sub, findall, search
from time import time


class Command(BaseCommand):
    help = '''
    It is a currency parser.
    Available parsers:
    privat
    mono
    aval
    nbu
    vkurse
    
    You can use all at once or combine.
    '''
    missing_args_message = '''
    Specify a parser.
    Available parsers:
    privat
    mono
    aval
    nbu
    vkurse
    
    You can use all at once or combine.
    '''

    def add_arguments(self, parser):
        parser.add_argument('sources', nargs='+', type=str)

    def handle(self, *args, **options):
        currency_codes = {
            'USD': 840,
            'EUR': 978,
            'RUB': 643,
            'RUR': 643
        }
        defined_sources = {
            'aval': self.pars_aval_ua,
            'mono': self.pars_api_monobank,
            'privat': self.pars_api_privatbank,
            'nbu': self.pars_bank_gov_ua,
            'vkurse': self.pars_vkurse_dp_ua,
        }
        for source in options['sources']:
            if source in defined_sources.keys():
                time_start = time()
                parse_method = defined_sources.get(source)
                parse_method(currency_codes, source)
                self.stdout.write(f'{source} parsed in {time() - time_start} second')
            else:
                self.stderr.write(f'"{source}" parser is not defined. Look in the help.')

    def print_exchange_rate(self, source, currency, buy, sale):
        self.stdout.write(f'{source} {currency} - buy: {buy} sale: {sale}')

    def save_exchange_rates(self, exchange_rates):
        # Takes [(currency, buy, sale, source_url), .....]
        obj_list = [ExchangeRate(currency=currency,
                                 buy=buy,
                                 sale=sale,
                                 source=source_url)
                    for currency, buy, sale, source_url in exchange_rates]
        ExchangeRate.objects.bulk_create(obj_list)

    def pars_vkurse_dp_ua(self, currency_codes, source):
        source_url = "http://vkurse.dp.ua/"
        session = HTMLSession()
        response = session.get(source_url)
        if response.status_code == 200:
            response.html.render()
            div_tags = response.html.find("div#dollar-section")
            exchange_rates = []
            for div_tag in div_tags:
                raw_currency = div_tag.find("div.cirlce-value")[0].text
                currency = search(r'[A-Z]+', raw_currency).group()
                buy, sale = [p_tag.text for p_tag in div_tag.find("p.pokupka-value")]
                self.print_exchange_rate(source, currency, buy, sale)
                exchange_rates.append((currency_codes.get(currency), buy, sale, source_url))
            self.save_exchange_rates(exchange_rates)

    def pars_bank_gov_ua(self, currency_codes, source):
        source_url = "https://bank.gov.ua/ua/markets/exchangerates"
        response = requests.get(source_url)
        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            exchange_rates = []
            for tr_tag in tree.xpath('//*[@id="exchangeRates"]/tbody/tr'):
                currency_txt = tr_tag.xpath('.//td')[1].text_content()
                if currency_txt in currency_codes:
                    currency = tr_tag.xpath('.//td')[0].text_content()
                    amount_currency = tr_tag.xpath('.//td')[2].text_content()
                    buy_sale = tr_tag.xpath('.//td')[4].text_content().replace(',', '.')
                    buy = sale = Decimal(buy_sale) / int(amount_currency)
                    self.print_exchange_rate(source, currency_txt, buy, sale)
                    exchange_rates.append((currency, buy, sale, source_url))
            self.save_exchange_rates(exchange_rates)

    def pars_aval_ua(self, currency_codes, source):
        source_url = "https://www.aval.ua/documents/currencies/kursy-valiut-v-kasakh-viddilen-banku"
        session = HTMLSession()
        response = session.get(source_url)
        if response.status_code == 200:
            response.html.render()
            link_to_pdf = response.html.find("a.download-file")[0].absolute_links.pop()
            web_file = urllib.request.urlopen(link_to_pdf)
            memory_file = BytesIO(web_file.read())
            pdf = PyPDF2.PdfFileReader(memory_file)
            page = pdf.getPage(0)
            raw_data = page.extractText()
            semi_raw_data = sub(r'[\s\t\n\r]+', ' ', raw_data)
            all_data = findall(r'([A-Z]+\s[\d.]+\s[\d.]+)', semi_raw_data)
            data_our_area = all_data[:len(all_data) // 2]
            exchange_rates = []
            for data in data_our_area:
                currency, buy, sale = data.split()
                if currency in currency_codes:
                    self.print_exchange_rate(source, currency, buy, sale)
                    exchange_rates.append((currency_codes.get(currency), buy, sale, source_url))
            self.save_exchange_rates(exchange_rates)

    def pars_api_privatbank(self, currency_codes, source):
        source_url = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5"
        response = requests.get(source_url)
        if response.status_code == 200:
            raw_data = response.json()
            exchange_rates = []
            for data in raw_data:
                if data['ccy'] in currency_codes:
                    currency, buy, sale = data['ccy'], data['buy'], data['sale']
                    self.print_exchange_rate(source, currency, buy, sale)
                    exchange_rates.append((currency_codes.get(currency), buy, sale, source_url))
            self.save_exchange_rates(exchange_rates)

    def pars_api_monobank(self, currency_codes, source):
        source_url = "https://api.monobank.ua//bank/currency"
        response = requests.get(source_url)
        if response.status_code == 200:
            raw_data = response.json()
            currency_codes_mirror = {v: k for k, v in currency_codes.items()}
            exchange_rates = []
            for data in raw_data:
                if data['currencyCodeA'] in currency_codes.values() \
                        and data['currencyCodeB'] == 980:
                    currency, buy, sale = data['currencyCodeA'], data['rateBuy'], data['rateSell']
                    currency_txt = currency_codes_mirror.get(currency)
                    self.print_exchange_rate(source, currency_txt, buy, sale)
                    exchange_rates.append((currency, buy, sale, source_url))
            self.save_exchange_rates(exchange_rates)
