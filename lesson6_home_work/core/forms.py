from django import forms

from core.models import Teacher, Student, Group
from core.tasks import send_contact_us_email


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'


class ContactUsForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField()
    message = forms.CharField(
        widget=forms.widgets.Textarea(attrs={})
    )

    def save(self):
        send_contact_us_email.delay(**self.cleaned_data)
