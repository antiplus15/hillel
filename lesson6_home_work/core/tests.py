from django.test import TestCase
from core.models import Student, Group, Teacher, Logger
from django.utils import timezone
from core.tasks import delete_logs_older_7_days, update_drf_tokens
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


# Create your tests here.

class StudentTestCase(TestCase):
    def setUp(self):
        Student.objects.create(
            name='Ivan18',
            date_of_birth=timezone.now().replace(
                year=timezone.now().year - 18,
            ).date()
        )

        Student.objects.create(
            name='Ivan17',
            date_of_birth=timezone.now().replace(
                year=timezone.now().year - 18,
                day=timezone.now().day + 1
            ).date()
        )

    def test_student_age_method(self):
        ivan18 = Student.objects.get(name='Ivan18')
        ivan17 = Student.objects.get(name='Ivan17')
        self.assertEqual(ivan18.age, 18)
        self.assertEqual(ivan17.age, 17)


class GroupTestCase(TestCase):
    def setUp(self):
        Student.objects.create(
            name='Ivan17',
            date_of_birth=timezone.now().replace(
                year=timezone.now().year - 17,
            ).date()
        )

        Student.objects.create(
            name='Ivan18',
            date_of_birth=timezone.now().replace(
                year=timezone.now().year - 18,
            ).date()
        )

        Student.objects.create(
            name='Ivan19',
            date_of_birth=timezone.now().replace(
                year=timezone.now().year - 19,
            ).date()
        )

        Teacher.objects.create(
            name='test_teacher',
            email='test@test.test'
        )

        Group.objects.create(
            name='test_group',
            teacher=Teacher.objects.get(
                name='test_teacher')
        ).students.set(Student.objects.filter(
            name__in=('Ivan17', 'Ivan18', 'Ivan19')))

    def test_group_get_statistic_method(self):
        group = Group.objects.get(name='test_group')
        self.assertEqual(group.get_statistic['max_age'], 19, )
        self.assertEqual(group.get_statistic['min_age'], 17)
        self.assertEqual(group.get_statistic['avg_age'], 18)
        self.assertEqual(group.get_statistic['student_count'], 3)


class CeleryTasksTestCase(TestCase):

    def test_task_delete_logs_older_7_days(self):
        #  creating "young" log
        Logger.objects.create(
            request_path='/test/',
            request_method='TEST',
            request_execution_time=1,
            created=timezone.now() - timezone.timedelta(days=6,
                                                        hours=23,
                                                        minutes=59,
                                                        seconds=59)
        )

        #  creating "old" log
        Logger.objects.create(
            request_path='/test/',
            request_method='TEST',
            request_execution_time=1,
            created=timezone.now() - timezone.timedelta(days=7,
                                                        seconds=1)
        )

        delete_logs_older_7_days()

        self.assertEqual(Logger.objects.count(), 1)

    def test_task_update_drf_tokens(self):
        # creating users and tokens
        Token.objects.create(
            user=User.objects.create(username='first_test_user',
                                     password='12345')
        )
        Token.objects.create(
            user=User.objects.create(username='second_test_user',
                                     password='12345')
        )
        Token.objects.create(
            user=User.objects.create(username='third_test_user',
                                     password='12345')
        )
        test_user_names = ['first_test_user',
                           'second_test_user',
                           'third_test_user']

        old_token_keys = {token.key for token in
                          Token.objects.filter(user__username__in=test_user_names)}

        update_drf_tokens()

        new_token_keys = {token.key for token in
                          Token.objects.filter(user__username__in=test_user_names)}

        self.assertFalse(old_token_keys & new_token_keys)
