from django import template
from core.models import Teacher
from django import template
from django.conf import settings
from os import listdir as os_listdir
register = template.Library()


@register.filter
def even_numbers(array_):
    return [x for x in array_ if x % 2 == 0]


@register.filter
def words_count(string):
    return len(string.split())


@register.filter
def add_form_id(form_field, instance_pk):
    attrs = {'form': f"form_{instance_pk}"}
    return form_field.as_widget(attrs=attrs)


@register.inclusion_tag('includes/random_teachers.html')
def random_teachers(count=5):
    teachers = Teacher.objects.order_by('?')[:count]
    return {'teachers': teachers}
