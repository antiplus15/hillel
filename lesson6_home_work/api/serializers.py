from abc import ABC

from rest_framework import serializers

from core.models import Student, Group, Teacher


# class StudentsSerializer(serializers.Serializer, ABC):
#     name = serializers.CharField()
#     date_of_birth = serializers.DateField()
#     age = serializers.IntegerField(read_only=True)
#
#     def create(self, validated_data):
#         return Student.objects.create(**validated_data)

class StudentsSerializer(serializers.ModelSerializer):
    age = serializers.IntegerField(read_only=True)

    class Meta:
        model = Student
        fields = '__all__'


class GroupsSerializer(serializers.ModelSerializer):
    statistic = serializers.JSONField(source='get_statistic', read_only=True)

    class Meta:
        model = Group
        fields = '__all__'


class TeachersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Teacher
        fields = '__all__'
