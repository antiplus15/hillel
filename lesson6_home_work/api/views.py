from django.shortcuts import render, get_object_or_404
from django.views import View
from rest_framework.viewsets import ViewSet, ModelViewSet
from api.serializers import StudentsSerializer, GroupsSerializer, TeachersSerializer
from core.models import Student, Group, Teacher
from django.http.response import JsonResponse
from rest_framework.permissions import IsAuthenticated
# Create your views here.


# class StudentsView(View):
#
#     def get(self, request):
#         students = Student.objects.all()
#         serializers = StudentsSerializer(students, many=True)
#         print(serializers.data)
#         return JsonResponse(serializers.data, safe=False)

# class StudentsView(ViewSet):
#     permission_classes = (IsAuthenticated, )
#
#     def list(self, request):
#         students = Student.objects.all()
#         serializer = StudentsSerializer(students, many=True)
#         return JsonResponse(serializer.data, safe=False)
#
#     def create(self, request):
#         serializer = StudentsSerializer(data=request.data)
#         if serializer.is_valid(serializer.data):
#             obj = serializer.save()
#             return JsonResponse(StudentsSerializer(obj.data), safe=False, status=201)
#         return JsonResponse(serializer.errors)
#
#     def update(self, request, pk):
#         student = get_object_or_404(Student, pk=pk)
#         serializer = StudentsSerializer(instance=student, data=request.data)
#         if serializer.is_valid():
#             obj = serializer.save()
#             return JsonResponse(StudentsSerializer(obj).data, safe=False, status=201)
#         return JsonResponse(serializer.errors)
#
#     def delete(self, request, pk):
#         student = get_object_or_404(Student, id=pk)
#         student.delete()
#         return JsonResponse({'status': 'success'}, status=204)

class StudentView(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Student.objects.all()
    serializer_class = StudentsSerializer


class GroupView(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Group.objects.all().select_related('teacher').prefetch_related('students')
    serializer_class = GroupsSerializer


class TeacherView(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Teacher.objects.all()
    serializer_class = TeachersSerializer
